import 'package:flutter_flux/flutter_flux.dart';
import 'actions.dart';

class UserStore extends Store {
  
  String _currentName = '';
  String get currentName => _currentName;

  UserStore() {    
    triggerOnAction(setCurrentNameAction, (String name) {
      _currentName = name;
    });
  }
}


