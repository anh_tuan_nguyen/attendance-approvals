import 'package:flutter_flux/flutter_flux.dart';
import '../models/stores.dart';

final StoreToken userStoreToken = new StoreToken(new UserStore());
final Action<String> setCurrentNameAction = new Action<String>();