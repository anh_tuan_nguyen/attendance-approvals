import 'package:flutter/material.dart';
import 'screens/login_screen.dart';
import 'screens/dashboard_screen.dart';
import 'screens/request_screen.dart';

class App extends StatelessWidget {
  Widget build(context) {
    return MaterialApp(
      //Route      
      routes: {        
        '': (context) => DashboardScreen(),        
        'second': (context) => RequestScreen(),
      },

      home: Scaffold(
        body: new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/images/bg_image.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: LoginScreen(),
        ),
      ),
    );
  }
}
