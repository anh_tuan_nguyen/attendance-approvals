import 'package:flutter/material.dart';
import 'dashboard_screen.dart';
import 'package:flutter_flux/flutter_flux.dart';
import '../models/stores.dart';
import '../models/actions.dart';

class LoginScreen extends StatefulWidget {
  createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen>
    with StoreWatcherMixin<LoginScreen> {
  UserStore userStore;
  String _userName = '';
  final textController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    userStore = listenToStore(userStoreToken);
  }

  Widget build(context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(30.0, 150.0, 30.0, 0.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Image.asset(
                  'assets/images/terralogic-logo.png',
                  width: 150.0,
                  height: 100.0,
                ),
                emailField(),
                passwordField(),
                Container(margin: EdgeInsets.only(top: 25.0)),
                loginButton()
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: textController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Username',
      ),
      validator: (String value) {
        if (!value.contains('@')) {
          return 'Please enter valid email';
        }
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Password',
      ),
      validator: (String value) {
        if (value.length < 4) {
          return 'Password must be at least 4 characters';
        }
      },
    );
  }

  Widget loginButton() {
    return RaisedButton(
      child: Text('Login'),
      color: Color.fromRGBO(74, 192, 230, 1.0),
      onPressed: () {
        setState(() {
          _userName = (textController.text).toString();
        });
        setCurrentNameAction(this._userName);

        if (formKey.currentState.validate()) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DashboardScreen()),
          );
          formKey.currentState.reset();
        }
      },
    );
  }
}
