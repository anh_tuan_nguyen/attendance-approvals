import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'dart:convert';

class RequestScreen extends StatefulWidget {
  createState() {
    return RequestState();
  }
}

class RequestState extends State<RequestScreen> {
  final items = List<String>.generate(100000, (i) => "Item $i");
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: appBar(),
          body: TabBarView(
            children: [
              requestTabBar(),
              approvalTabBar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget appBar() {
    return AppBar(
      leading: IconButton(
        tooltip: 'Dashboard',
        icon: const Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pushNamed(context, '');
        },
      ),
      title: Text(
        'Reimbursement',
      ),
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.sort),
          tooltip: 'Sort',
          onPressed: () {},
        ),
      ],
      bottom: TabBar(
        tabs: [
          tabbarItem('Requests'),
          tabbarItem('Approvals'),
        ],
      ),
    );
  }

  Widget tabbarItem(String itemName) {
    return Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(itemName, style: TextStyle(fontSize: 16.0)),
        ],
      ),
    );
  }

  Widget requestTabBar() {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return listTile(items[index], Icon(Icons.map), Icon(Icons.face));
        });
  }

  Widget approvalTabBar() {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return listTile(items[index], Icon(Icons.games), Icon(Icons.backup));
        });
  }

  Widget listTile(leading, title, trailing) {
    return ListTile(
      leading: Text(leading),
      title: title,
      trailing: trailing,
    );
  }
}
