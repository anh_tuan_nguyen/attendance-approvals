import 'package:flutter/material.dart';
import '../models/stores.dart';
import '../models/actions.dart';
import 'package:flutter_flux/flutter_flux.dart';

class DashboardScreen extends StatefulWidget {  
  createState() {
    return DashboardScreenState();
  }
}
class DashboardScreenState extends State<DashboardScreen> 
    with StoreWatcherMixin<DashboardScreen> {

  UserStore userStore;
  String name;
  
  @override
  void initState() {
    super.initState();
    userStore = listenToStore(userStoreToken);
  }    
  
  Widget build(context) {   
    this.name = this.userStore.currentName;    
    return MaterialApp(
      home: Scaffold(
        body: new Container(
          child: Column(
            children: <Widget>[
              welcomeView(),
              Container(
                margin: EdgeInsets.only(top: 100.0),
                child: Row(
                  children: <Widget>[
                    buttonView(context),
                    textView(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget welcomeView() {
    return Stack(
      alignment: Alignment(0.0, 0.0),
      children: <Widget>[
        Image.asset(
          'assets/images/header_image.png',
        ),
        Container(
          child: Column(
            children: <Widget>[
              Text(
                'Welcome $name',
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  buttonView(context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 20.0),
            child: FloatingActionButton(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text("15")],
              ),
              onPressed: () {
                Navigator.pushNamed(context, 'second');
              },
            ),
          ),
        ],
      ),
    );
  }

  textView() {
    return Expanded(
      child: Text(
        "ATTENDANCE \n APPROVALS",
        textAlign: TextAlign.left,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }
}
